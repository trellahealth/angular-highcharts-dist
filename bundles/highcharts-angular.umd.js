(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('highcharts-angular', ['exports', '@angular/core'], factory) :
    (factory((global['highcharts-angular'] = {}),global.ng.core));
}(this, (function (exports,core) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var HighchartsChartComponent = /** @class */ (function () {
        function HighchartsChartComponent(el, _zone // #75
        ) {
            this.el = el;
            this._zone = _zone;
            this.updateChange = new core.EventEmitter(true);
            this.chartInstance = new core.EventEmitter();
        }
        Object.defineProperty(HighchartsChartComponent.prototype, "options", {
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                this.optionsValue = val;
                this.wrappedUpdateOrCreateChart();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HighchartsChartComponent.prototype, "update", {
            set: /**
             * @param {?} val
             * @return {?}
             */ function (val) {
                if (val) {
                    this.wrappedUpdateOrCreateChart();
                    this.updateChange.emit(false); // clear the flag after update
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        HighchartsChartComponent.prototype.wrappedUpdateOrCreateChart = /**
         * @return {?}
         */
            function () {
                var _this = this;
                // #75
                if (this.runOutsideAngular) {
                    this._zone.runOutsideAngular(function () {
                        _this.updateOrCreateChart();
                    });
                }
                else {
                    this.updateOrCreateChart();
                }
            };
        /**
         * @return {?}
         */
        HighchartsChartComponent.prototype.updateOrCreateChart = /**
         * @return {?}
         */
            function () {
                if (this.chart && this.chart.update) {
                    this.chart.update(this.optionsValue, true, this.oneToOne || false);
                }
                else {
                    this.chart = ( /** @type {?} */(this.Highcharts))[this.constructorType || 'chart'](this.el.nativeElement, this.optionsValue, this.callbackFunction || null);
                    // emit chart instance on init
                    this.chartInstance.emit(this.chart);
                }
            };
        /**
         * @return {?}
         */
        HighchartsChartComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                // #44
                if (this.chart && this.chart.chartHeight) { // #56
                    // #56
                    this.chart.destroy();
                    this.chart = null;
                }
            };
        HighchartsChartComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'highcharts-chart',
                        template: ''
                    }] }
        ];
        /** @nocollapse */
        HighchartsChartComponent.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
                { type: core.NgZone, },
            ];
        };
        HighchartsChartComponent.propDecorators = {
            "Highcharts": [{ type: core.Input },],
            "constructorType": [{ type: core.Input },],
            "callbackFunction": [{ type: core.Input },],
            "oneToOne": [{ type: core.Input },],
            "runOutsideAngular": [{ type: core.Input },],
            "options": [{ type: core.Input },],
            "update": [{ type: core.Input },],
            "updateChange": [{ type: core.Output },],
            "chartInstance": [{ type: core.Output },],
        };
        return HighchartsChartComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var HighchartsChartModule = /** @class */ (function () {
        function HighchartsChartModule() {
        }
        HighchartsChartModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [HighchartsChartComponent],
                        exports: [HighchartsChartComponent]
                    },] }
        ];
        /** @nocollapse */
        HighchartsChartModule.ctorParameters = function () { return []; };
        return HighchartsChartModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.HighchartsChartModule = HighchartsChartModule;
    exports.HighchartsChartComponent = HighchartsChartComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGNoYXJ0cy1hbmd1bGFyLnVtZC5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vaGlnaGNoYXJ0cy1hbmd1bGFyL2xpYi9oaWdoY2hhcnRzLWNoYXJ0LmNvbXBvbmVudC50cyIsIm5nOi8vaGlnaGNoYXJ0cy1hbmd1bGFyL2xpYi9oaWdoY2hhcnRzLWNoYXJ0Lm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uRGVzdHJveSwgT3V0cHV0LCBOZ1pvbmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIEhpZ2hjaGFydHMgZnJvbSAnaGlnaGNoYXJ0cyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2hpZ2hjaGFydHMtY2hhcnQnLFxuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgSGlnaGNoYXJ0c0NoYXJ0Q29tcG9uZW50IGltcGxlbWVudHMgT25EZXN0cm95IHtcbiAgQElucHV0KCkgSGlnaGNoYXJ0czogdHlwZW9mIEhpZ2hjaGFydHM7XG4gIEBJbnB1dCgpIGNvbnN0cnVjdG9yVHlwZTogc3RyaW5nO1xuICBASW5wdXQoKSBjYWxsYmFja0Z1bmN0aW9uOiBIaWdoY2hhcnRzLkNoYXJ0Q2FsbGJhY2tGdW5jdGlvbjtcbiAgQElucHV0KCkgb25lVG9PbmU6IGJvb2xlYW47IC8vICMyMFxuICBASW5wdXQoKSBydW5PdXRzaWRlQW5ndWxhcjogYm9vbGVhbjsgLy8gIzc1XG5cbiAgQElucHV0KCkgc2V0IG9wdGlvbnModmFsOiBIaWdoY2hhcnRzLk9wdGlvbnMpIHtcbiAgICB0aGlzLm9wdGlvbnNWYWx1ZSA9IHZhbDtcbiAgICB0aGlzLndyYXBwZWRVcGRhdGVPckNyZWF0ZUNoYXJ0KCk7XG4gIH1cbiAgQElucHV0KCkgc2V0IHVwZGF0ZSh2YWw6IGJvb2xlYW4pIHtcbiAgICBpZiAodmFsKSB7XG4gICAgICB0aGlzLndyYXBwZWRVcGRhdGVPckNyZWF0ZUNoYXJ0KCk7XG4gICAgICB0aGlzLnVwZGF0ZUNoYW5nZS5lbWl0KGZhbHNlKTsgLy8gY2xlYXIgdGhlIGZsYWcgYWZ0ZXIgdXBkYXRlXG4gICAgfVxuICB9XG5cbiAgQE91dHB1dCgpIHVwZGF0ZUNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4odHJ1ZSk7XG4gIEBPdXRwdXQoKSBjaGFydEluc3RhbmNlID0gbmV3IEV2ZW50RW1pdHRlcjxIaWdoY2hhcnRzLkNoYXJ0PigpOyAvLyAjMjZcblxuICBwcml2YXRlIGNoYXJ0OiBIaWdoY2hhcnRzLkNoYXJ0O1xuICBwcml2YXRlIG9wdGlvbnNWYWx1ZTogSGlnaGNoYXJ0cy5PcHRpb25zO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZWw6IEVsZW1lbnRSZWYsXG4gICAgcHJpdmF0ZSBfem9uZTogTmdab25lIC8vICM3NVxuICApIHt9XG5cbiAgd3JhcHBlZFVwZGF0ZU9yQ3JlYXRlQ2hhcnQoKSB7IC8vICM3NVxuICAgIGlmICh0aGlzLnJ1bk91dHNpZGVBbmd1bGFyKSB7XG4gICAgICB0aGlzLl96b25lLnJ1bk91dHNpZGVBbmd1bGFyKCgpID0+IHtcbiAgICAgICAgdGhpcy51cGRhdGVPckNyZWF0ZUNoYXJ0KClcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnVwZGF0ZU9yQ3JlYXRlQ2hhcnQoKTtcbiAgICB9XG4gIH1cblxuICB1cGRhdGVPckNyZWF0ZUNoYXJ0KCkge1xuICAgIGlmICh0aGlzLmNoYXJ0ICYmIHRoaXMuY2hhcnQudXBkYXRlKSB7XG4gICAgICB0aGlzLmNoYXJ0LnVwZGF0ZSh0aGlzLm9wdGlvbnNWYWx1ZSwgdHJ1ZSwgdGhpcy5vbmVUb09uZSB8fCBmYWxzZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuY2hhcnQgPSAodGhpcy5IaWdoY2hhcnRzIGFzIGFueSlbdGhpcy5jb25zdHJ1Y3RvclR5cGUgfHwgJ2NoYXJ0J10oXG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudCxcbiAgICAgICAgdGhpcy5vcHRpb25zVmFsdWUsXG4gICAgICAgIHRoaXMuY2FsbGJhY2tGdW5jdGlvbiB8fCBudWxsXG4gICAgICApO1xuXG4gICAgICAvLyBlbWl0IGNoYXJ0IGluc3RhbmNlIG9uIGluaXRcbiAgICAgIHRoaXMuY2hhcnRJbnN0YW5jZS5lbWl0KHRoaXMuY2hhcnQpO1xuICAgIH1cbiAgfVxuXG4gIG5nT25EZXN0cm95KCkgeyAvLyAjNDRcbiAgICBpZiAodGhpcy5jaGFydCAmJiB0aGlzLmNoYXJ0LmNoYXJ0SGVpZ2h0KSB7ICAvLyAjNTZcbiAgICAgIHRoaXMuY2hhcnQuZGVzdHJveSgpO1xuICAgICAgdGhpcy5jaGFydCA9IG51bGw7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7SGlnaGNoYXJ0c0NoYXJ0Q29tcG9uZW50fSBmcm9tICcuL2hpZ2hjaGFydHMtY2hhcnQuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbIEhpZ2hjaGFydHNDaGFydENvbXBvbmVudCBdLFxuICBleHBvcnRzOiBbIEhpZ2hjaGFydHNDaGFydENvbXBvbmVudCBdXG59KVxuZXhwb3J0IGNsYXNzIEhpZ2hjaGFydHNDaGFydE1vZHVsZSB7fVxuIl0sIm5hbWVzIjpbIkV2ZW50RW1pdHRlciIsIkNvbXBvbmVudCIsIkVsZW1lbnRSZWYiLCJOZ1pvbmUiLCJJbnB1dCIsIk91dHB1dCIsIk5nTW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7UUErQkUsa0NBQ1UsSUFDQTs7WUFEQSxPQUFFLEdBQUYsRUFBRTtZQUNGLFVBQUssR0FBTCxLQUFLO2dDQVJVLElBQUlBLGlCQUFZLENBQVUsSUFBSSxDQUFDO2lDQUM5QixJQUFJQSxpQkFBWSxFQUFvQjtTQVExRDs4QkFwQlMsNkNBQU87Ozs7MEJBQUMsR0FBdUI7Z0JBQzFDLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDO2dCQUN4QixJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQzs7Ozs7OEJBRXZCLDRDQUFNOzs7OzBCQUFDLEdBQVk7Z0JBQzlCLElBQUksR0FBRyxFQUFFO29CQUNQLElBQUksQ0FBQywwQkFBMEIsRUFBRSxDQUFDO29CQUNsQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDL0I7Ozs7Ozs7O1FBY0gsNkRBQTBCOzs7WUFBMUI7Z0JBQUEsaUJBUUM7O2dCQVBDLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO29CQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDO3dCQUMzQixLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtxQkFDM0IsQ0FBQyxDQUFDO2lCQUNKO3FCQUFNO29CQUNMLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2lCQUM1QjthQUNGOzs7O1FBRUQsc0RBQW1COzs7WUFBbkI7Z0JBQ0UsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFO29CQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQyxDQUFDO2lCQUNwRTtxQkFBTTtvQkFDTCxJQUFJLENBQUMsS0FBSyxHQUFHLG1CQUFDLElBQUksQ0FBQyxVQUFpQixHQUFFLElBQUksQ0FBQyxlQUFlLElBQUksT0FBTyxDQUFDLENBQ3BFLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUNyQixJQUFJLENBQUMsWUFBWSxFQUNqQixJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUM5QixDQUFDOztvQkFHRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3JDO2FBQ0Y7Ozs7UUFFRCw4Q0FBVzs7O1lBQVg7O2dCQUNFLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRTs7b0JBQ3hDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2lCQUNuQjthQUNGOztvQkEvREZDLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsa0JBQWtCO3dCQUM1QixRQUFRLEVBQUUsRUFBRTtxQkFDYjs7Ozs7d0JBTm1CQyxlQUFVO3dCQUEwQ0MsV0FBTTs7OzttQ0FRM0VDLFVBQUs7d0NBQ0xBLFVBQUs7eUNBQ0xBLFVBQUs7aUNBQ0xBLFVBQUs7MENBQ0xBLFVBQUs7Z0NBRUxBLFVBQUs7K0JBSUxBLFVBQUs7cUNBT0xDLFdBQU07c0NBQ05BLFdBQU07O3VDQTFCVDs7Ozs7OztBQ0FBOzs7O29CQUdDQyxhQUFRLFNBQUM7d0JBQ1IsWUFBWSxFQUFFLENBQUUsd0JBQXdCLENBQUU7d0JBQzFDLE9BQU8sRUFBRSxDQUFFLHdCQUF3QixDQUFFO3FCQUN0Qzs7OztvQ0FORDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9